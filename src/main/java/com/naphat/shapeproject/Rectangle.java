/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naphat.shapeproject;

import static com.naphat.shapeproject.Circle.pi;

/**
 *
 * @author ASUS
 */
public class Rectangle {
    double w;
    double l;
    
    public double calArea2() {
        return w * l ;    
    }
    public double getW() {
        return w;
    }
    public double getL() {
        return l;
    }
    public void setW(double w) {
        if(w <= 0){
            System.out.println("Error: wide must more than 0 !!");
            return ;
        }
        this.w = w;
    }
    public void setL(double l) {
        if(l <= 0){
            System.out.println("Error: wide must more than 0 !!");
            return ;
        }
        this.l = l;
    }
}
