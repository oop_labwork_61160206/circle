/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.naphat.shapeproject;

/**
 *
 * @author ASUS
 */
public class Circle {
    double r;
    static final double pi = 22.0/7;
    public Circle (double r) {
        this.r = r;
    }
    public double calArea() {
        return pi * r * r ;    
    }
    public double getR() {
        return r;
    }
    public void setR(double r) {
        if(r <= 0){
            System.out.println("Error: Radius must more than 0 !!");
            return ;
        }
        this.r = r;
    }
     public String toString() {
         return "Area of circle1(r = " + this.getR() + ") is " + this.calArea();
     }
}
